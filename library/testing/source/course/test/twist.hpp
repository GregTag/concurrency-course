#pragma once

#include "twist/stress.hpp"
#include "twist/model.hpp"
#include "twist/random.hpp"

#include <chrono>

#include <wheels/test/framework.hpp>

////////////////////////////////////////////////////////////////////////////////

// Stress testing

#define TWIST_STRESS_TEST(name, time_budget) \
    void TwistStressTestBody##name();        \
    TEST(name, ::wheels::test::TestOptions().TimeLimit(time_budget)) {  \
      ::course::twist::stress::Test([] {                \
        TwistStressTestBody##name(); \
      }, {time_budget});                    \
    }                                     \
    void TwistStressTestBody##name()

////////////////////////////////////////////////////////////////////////////////

// Stateless model checking

#define TWIST_MODEL(name, params) \
    void TwistModelBody##name();        \
    TEST(name, ::wheels::test::TestOptions().TimeLimit(std::chrono::seconds(60))) {                \
      static_assert(::course::twist::model::Supported());                                   \
      ::course::twist::model::Check([] {                \
        TwistModelBody##name(); \
      }, params);                    \
    }                                     \
    void TwistModelBody##name()

////////////////////////////////////////////////////////////////////////////////

// Randomized checking

#define TWIST_RANDOM_CHECK(name, time_budget) \
  void TwistRandomCheckBody##name();        \
  TEST(name, ::wheels::test::TestOptions().TimeLimit(time_budget)) { \
    static_assert(::course::twist::random::Supported());              \
    ::course::twist::random::Check([] {                \
      TwistRandomCheckBody##name(); \
    }, {time_budget});                    \
  }                                     \
  void TwistRandomCheckBody##name()
