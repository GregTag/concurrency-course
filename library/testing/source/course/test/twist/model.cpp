#include "model.hpp"

#include "sim/result.hpp"

#include <twist/sim.hpp>

#include <wheels/core/panic.hpp>
#include <wheels/test/framework.hpp>

#include <fmt/core.h>

namespace course::twist {

namespace model {

struct ExploreResult {
  std::optional<::twist::sim::Result> failure;
  size_t sim_count;
};

using ModelChecker = ::twist::sim::sched::DfsScheduler;

static ModelChecker::Params ToModelCheckerParams(model::Params test_params) {
  ModelChecker::Params params;

  params.max_preemptions = test_params.max_preemptions;
  params.max_steps = test_params.max_steps;
  params.spurious_failures = test_params.spurious_failures;
  params.spurious_wakeups = test_params.spurious_wakeups;

  return params;

}

ExploreResult Explore(TestBody body, ModelChecker& checker) {
  size_t sim_count = 0;

  do {
    ++sim_count;

    ::twist::sim::Simulator simulator{&checker};

    auto result = simulator.Run(body);

    // fmt::println("Steps: {}", result.iters);

    if (result.Failure()) {
      return {result, sim_count};
    }
  } while (checker.NextSchedule());

  return {{}, sim_count};  // Ok
}

struct Replay {
  ::twist::sim::sched::Schedule schedule;
  ::twist::sim::Digest digest;
};

Replay TryRecord(TestBody body, ModelChecker& checker) {
  ::twist::sim::sched::Recorder recorder{&checker};
  ::twist::sim::Simulator simulator{&recorder};
  auto result = simulator.Run(body);
  return {recorder.GetSchedule(), result.digest};
}

void Check(TestBody body, Params test_params) {
  if (!::twist::sim::DetCheck(body)) {
    FAIL_TEST("Test routine is not deterministic");
  }

  ModelChecker checker{ToModelCheckerParams(test_params)};

  // Explore test
  auto exp = Explore(body, checker);

  if (exp.failure) {
    ::twist::sim::Result failure = *exp.failure;

    auto replay = TryRecord(body, checker);

    if (replay.digest != failure.digest) {
      WHEELS_PANIC("Twist error: failed to record simulation replay");
    }

    ::twist::sim::Print(body, replay.schedule);

    FAIL_TEST(twist::sim::FormatFailure(failure));

  } else {
    // TODO: print report
    fmt::println("Model checking completed: {} executions", exp.sim_count);

    return;  // Passed
  }
}

}  // namespace model

}  // namespace course::twist
