#pragma once

#include <cstdlib>

namespace exe::threads {

class WaitGroup {
 public:
  // += count
  void Add(size_t /*count*/) {
    // Not implemented
  }

  // =- 1
  void Done() {
    // Not implemented
  }

  // == 0
  // One-shot
  void Wait() {
    // Not implemented
  }

 private:
  // ???
};

}  // namespace exe::threads
