# Интрузивные задачи

## Пререквизиты

- [tasks/manual](/tasks/tasks/manual)
- [желательно] [mutex/queue](/tasks/mutex/queue)  
- [опционально] [futures/fun](/tasks/futures/fun)

🛑 Приступайте к этой части **только если** вы можете написать собственный `std::function<void()>`.

---

Пока планирование задачи в экзекутор – это (в общем случае) динамическая аллокация контейнера для лямбды (назовем его _состоянием задачи_) для type erasure внутри `fu2::unique_function`.

![Non-intrusive tasks](https://gitlab.com/Lipovsky/concurrency-course-media/-/raw/main/tasks/executors/non-intrusive-tasks.png)

При этом главным "пользователям" экзекуторов – stackful файберам, stackless корутинам, фьючам – динамические аллокации при планировании не требуются: они могут управлять состоянием своих задач эффективнее. 

## Интрузивные задачи

Мы избавимся от динамических аллокаций памяти в экзекуторах, сделав задачи _интрузивными_.

Во-первых, спрячем конкретный тип задачи от планировщика за интерфейсом `ITask`:

```cpp
// exe/executors/task.hpp

struct ITask {
  virtual ~ITask() = default;
  
  virtual void Run() noexcept = 0;
};
```

За этим интерфейсом может находиться
- `Fiber`
- awaiter для stackless корутины
- служебный контейнер для лямбды пользователя для свободной функции `Submit`

Задачам нужно будет укладываться в очереди, так что встроим в них указатель:

```cpp
// exe/executors/task.hpp

struct TaskBase 
    : ITask,
      wheels::IntrusiveForwardListNode<TaskBase> {
  //
};
```

Метод `Submit` у `IExecutor` будет принимать `TaskBase*`:

```cpp
// exe/executors/executor.hpp

struct IExecutor {
  virtual ~IExecutor() = default;
  
  virtual void Submit(TaskBase* task) = 0;
};
```

## Пример

В этом примере нет ни одной динамической аллокации памяти:

```cpp
void IntrusiveTask() {
  using namespace exe;
  
  struct HelloTask : task::TaskBase {
    void Run() noexcept override {
      fmt::println("Hello, world!");
    }
  };
  
  task::ManualExecutor manual;
  
  HelloTask hello{};
  
  manual.Submit(&hello);
  manual.Drain();
}
```


Итого, в памяти выстроится следующая конструкция:

![Intrusive tasks](https://gitlab.com/Lipovsky/concurrency-course-media/-/raw/main/tasks/executors/intrusive-tasks.png)


- Файберы, корутины и фьючи будут использовать метод `Submit` у `IExecutor` напрямую, избегая ненужных аллокаций,
- Свободная функция `Submit` будет аллоцировать для лямбд служебную задачу-контейнер, которая будет самоуничтожаться при завершении.

## Задание

- Перепишите экзекуторы для работы с интрузивными задачами
- Сделайте файберы интрузивными задачами и избавьтесь от динамических аллокаций памяти при их планировании
