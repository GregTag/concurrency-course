#include <exe/thread_pool.hpp>
#include <exe/wait_group.hpp>

#include <course/twist/test.hpp>

#include <twist/test/race.hpp>
#include <twist/test/budget.hpp>
#include <twist/test/yield.hpp>

#include <twist/ed/stdlike/atomic.hpp>
#include <twist/ed/stdlike/thread.hpp>

#include <atomic>

void TestOneTask() {
  ThreadPool pool{4};

  pool.Start();

  while (twist::test::KeepRunning()) {
    WaitGroup wg;

    wg.Add(1);
    pool.Submit([&wg]() {
      wg.Done();
    });

    wg.Wait();
  }

  pool.Stop();
}

void TestSeries() {
  ThreadPool pool{1};

  pool.Start();

  size_t iter = 0;

  while (twist::test::KeepRunning()) {
    ++iter;
    const size_t tasks = 1 + iter % 3;

    WaitGroup wg;
    for (size_t i = 0; i < tasks; ++i) {
      wg.Add(1);
      pool.Submit([&wg] {
        wg.Done();
      });
    }

    wg.Wait();
  }

  pool.Stop();
}

void TestCurrent() {
  ThreadPool pool{2};

  pool.Start();

  while (twist::test::KeepRunning()) {
    WaitGroup wg;

    wg.Add(1);
    pool.Submit([&] {
      wg.Add(1);
      ThreadPool::Current()->Submit([&] {
        wg.Done();
      });
      wg.Done();
    });

    wg.Wait();
  }

  pool.Stop();
}

TEST_SUITE(WaitIdle) {
  TWIST_TEST(OneTask, 5s) {
    TestOneTask();
  }

  TWIST_TEST(Series, 5s) {
    TestSeries();
  }

  TWIST_TEST(Current, 5s) {
    TestCurrent();
  }
}

RUN_ALL_TESTS()
