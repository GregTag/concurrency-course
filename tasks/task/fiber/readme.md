# Пул файберов

## Пререквизиты

- [fibers/mutex](/tasks/fibers/mutex)
- [futures/lazy](/tasks/futures/lazy)
- [tasks/scheduler](/tasks/tasks/scheduler)  
- [tasks/manual-fibers](/tasks/tasks/manual-fibers)

---

## ThreadPool

```cpp
// Конфигурируем среду исполнения

// ResourceManager отвечает за аллокацию стеков
fibers::ResourceManager resource_manager{};
manager.SetStackSize(8_MiB);

// TaskRunner отвечает за запуск задач в файберах
fibers::TaskRunner task_runner{resource_manager};

executors::pools::fast::ThreadPool pool{4};
pool.SetTaskRunner(task_runner);

pool.Start();

{    
  auto task = futures::Submit(pool, [] {
    for (size_t i = 0; i < 128; ++i) {
      fibers::Yield();
    }
    return result::Ok();
  });
    
  std::move(task) | futures::Get();
}

pool.Stop();
```

Таким образом, пользователю никогда не нужно запускать файберы.

Пользователь планирует **задачи** через API фьюч, и эти задачи исполняются в служебных файберах, что позволяет
- задачам останавливаться (а значит пользователю писать код с мьютексами и каналами)
- рантайму эффективно переиспользовать файберы

## `Manual`

Напишите `ManualExecutor`, который запускает задачи в файберах.

### Пример

```cpp
void ManualFibersExample() {
  using namespace exe;
  
  fibers::executors::ManualExecutor manual;

  auto chain = futures::Submit(manual, [] {
    for (size_t i = 0; i < 10; ++i) {
      fibers::Yield();
    }
    return result::Ok();
  }) | futures::Manual();
  
  chain.Start();
  
  manual.Drain();
  
  manual.Stop();
}
```

### Требования к реализации

#### Пулинг

Экзекутор не должен заводить файбер на каждую новую задачу, он должен переиспользовать
уже созданные файберы

```cpp
void PoolingExample() {
  fibers::executors::ManualExecutor manual;
  
  executors::Submit(pool, [] {
    fibers::Yield();
  });
  
  manual.Drain();
  
  executors::Submit(pool, [] {
    fibers::Yield();
  });
  
  manual.Drain();
  
  assert(manual.FiberCount(), 1);
  
  manual.Stop();
}
```

#### Переключения контекста

Пользователь не должен платить переключениями контекста за
запуск задач, которые не останавливаются:

```cpp
void AlmostZeroOverheadExample() {
  fibers::executors::ManualExecutor manual;

  for (size_t i = 0; i < 1024; ++i) {
    executors::Submit(manual, [] {});
  }

  // На исполнение запланированных выше задач потребуется
  // O(1) переключений контекста
  manual.Drain();
  
  manual.Stop();
}
```