#include "../wait_group.hpp"

#include <course/test/twist.hpp>

#include <twist/ed/std/thread.hpp>

#include <twist/build.hpp>

static_assert(twist::build::Thr());

TEST_SUITE(WaitGroup) {
  TWIST_RANDOM_CHECK(Storage, 10s) {
    // Help AddressSanitizer
    WaitGroup* wg = new WaitGroup{};

    wg->Add(1);
    twist::ed::std::thread t([&wg] {
      wg->Done();
    });

    wg->Wait();
    delete wg;

    t.join();
  }
}

RUN_ALL_TESTS()
