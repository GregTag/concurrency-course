#include "atomic.hpp"

Atomic::Value Atomic::Load(MemoryOrder mo) {
  switch (mo) {
    case MemoryOrder::Relaxed:
      return AtomicLoadRelaxed(&storage_);
    case MemoryOrder::Acquire:
      return AtomicLoadAcquire(&storage_);
    case MemoryOrder::SeqCst:
      return AtomicLoadSeqCst(&storage_);
    default:
      return 0;  // UB
  }
}

void Atomic::Store(Value value, MemoryOrder mo) {
  switch (mo) {
    case MemoryOrder::Relaxed:
      AtomicStoreRelaxed(&storage_, value);
      break;
    case MemoryOrder::Release:
      AtomicStoreRelease(&storage_, value);
      break;
    case MemoryOrder::SeqCst:
      AtomicStoreSeqCst(&storage_, value);
      break;
    default:
      break;  // UB
  }
}

Atomic::Value Atomic::Exchange(Value new_value, MemoryOrder /*mo*/) {
  return AtomicExchangeSeqCst(&storage_, new_value);
}
