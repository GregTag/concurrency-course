# Race Detector

В этой задаче вы напишите детектор гонок для симулятора потоков на основе [TinyFibers](https://gitlab.com/Lipovsky/tinyfibers).

Ваша реализация детектора может быть даже более аккуратной, чем [ThreadSanitizer](https://clang.llvm.org/docs/ThreadSanitizer.html)! Она сможет

1) гарантированно обнаруживать все возникающие в исполнении гонки,
2) поддерживать `std::atomic_thread_fence`.

## Симулятор

Симулятор однопоточный

- `sim::Thread` ~ `std::thread`
- `sim::Mutex` ~ `std::mutex`
- `sim::Atomic` ~ `std::atomic`

Потоки в симуляции планируются на потоке, который вызывает функцию `sim::RunScheduler`

### `SwitchTo`

Тесты в этой задаче будут использовать функцию `sim::SwitchTo(sim::ThreadId)` для ручного управления планировщиком.


## Инструментация

Пользователь сам будет инструментировать код:

- `sim::Atomic<T>` – переменная, используемая для синхронизации, ~ `std::atomic<T>`
- `sim::Shared<T>` – разделяемая переменная, конфликтующие обращения к которой должны быть упорядочены через `Atomic` / производные примитивы синхронизации.

### `Atomic`

Конфликтующие обращения не упорядочены.

Шаблон `Atomic<T>` повторяет интерфейс `std::atomic`:

- `Load`,
- `Store`,
- `Exchange`,
- `CompareExchange`,

### `Shared`

Переменные, на которых будут проверяться гонки, должны быть аннотированы как `Shared<T>`.

Конфликтующие обращения должны быть упорядочены с помощью `Atomic` / `Thread`.

Шаблон `Shared<T>` поддерживает

- `operator=(T&&)` – запись
- `operator T() const` – чтение

Для отчета об ошибке полезно запоминать позицию в исходном коде для старого / текущего обращения, поэтому
`Shared` также имеет методы `void Write(T)` и `T Read() const`.

### Пример

```cpp
sim::RunScheduler([] {
  // Message passing
  sim::Shared<int> data{0};
  sim::Atomic<bool> flag{false};
  
  sim::Thread thr = sim::Spawn([] {
    data = 42;  // Or: data.Write(42);
    flag.Store(true, sim::MemoryOrder::Release);
  });
  
  while (!flag.Load(sim::MemoryOrder::Relaxed)) {  // ???
    sim::Yield();
  }
  // Or: int d = data.Read()
  int d = data;  // <- Data race detected
  
  thr.Join();  // Never happens
});
```

## Memory orders

Мы поддержим следующие `MemoryOrder`:

- `Acquire`
- `Release`
- `AcqRel`
- `Relaxed`

### Consume

Мы **не будем поддерживать** `std::memory_order::consume`.

В отсутствии `consume` под _happens-before_ мы понимаем [_simply happens-before_](https://eel.is/c++draft/intro.races#11).

## Release Sequences

Бонусный уровень!

Поддержите [release sequence](https://eel.is/c++draft/intro.races#def:release_sequence).

## Fences

Продвинутый уровень!

Поддержите функцию `sim::AtomicThreadFence(std::memory_order)`:

```cpp
sim::RunScheduler([] {
  sim::Shared<int> data{0};
  sim::Atomic<bool> flag{false};
  
  sim::Thread thr = sim::Spawn([] {
    data = 42;  // Or: data.Write(42);
    flag.Store(true, sim::MemoryOrder::Release);
  });
  
  while (!flag.Load(sim::MemoryOrder::Relaxed)) {  // ???
    sim::Yield();
  }

  sim::AtomicThreadFence(sim::MemoryOrder::Acquire);  // synchronizes-with flag.Store
  int d = data;  // <- Data race detected
  
  thr.Join();  // Never happens
});
```

## Сценарии

Ваш детектор должен поддерживать следующие сценарии:

- Message passing
- SpinLock
- SharedPtr
- Treiber stack
- Hazard pointers

## C++ MM

Релевантная часть модели памяти C++20:

- [conflict](https://eel.is/c++draft/intro.races#def:conflict)
- [data race](https://eel.is/c++draft/intro.races#def:data_race)
- [sequenced before](https://eel.is/c++draft/intro.execution#def:sequenced_before)
- [release sequence](https://eel.is/c++draft/intro.races#def:release_sequence)
- [synchronizes-with](https://eel.is/c++draft/atomics.order#2)
- [happens-before](https://eel.is/c++draft/intro.races#def:simply_happens_before)


## References

- [Vector clocks](https://en.wikipedia.org/wiki/Vector_clock)
- [ThreadSanitizer Runtime Library](https://github.com/llvm/llvm-project/tree/main/compiler-rt/lib/tsan/rtl)
