#include "../coroutine.hpp"

#include <fmt/core.h>

#include <cassert>

int main() {
  Coroutine coro([](auto self) {
    // Step 1
    self.Suspend();
    // Step 2
  });

  coro.Resume();  // Step 1
  coro.Resume();  // Step 2

  assert(coro.IsCompleted());

  return 0;
}
