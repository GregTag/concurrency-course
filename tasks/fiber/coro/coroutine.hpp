#pragma once

#include <sure/context.hpp>
#include <sure/stack.hpp>

#include <function2/function2.hpp>

#include <exception>

// Toy stackful coroutine

class Coroutine {
 public:
  class SuspendContext {
   public:
    SuspendContext(Coroutine* host)
      : host_(host) {
    }

    void Suspend() {
      host_->Suspend();
    }

   private:
    Coroutine* host_;
  };

  using Body = fu2::unique_function<void(SuspendContext)>;

  explicit Coroutine(Body body);

  void Resume();

  bool IsCompleted() const;

 private:
  friend class Context;

  void Suspend();

 private:
  // ???
};
